import configparser

config = configparser.ConfigParser()

config['DEFAULT'] = {
    'abc' : "/usr/bin/berkeley-abc",
    'abc_rc' : "./abc.rc",
    'abc_simplify' : "./simplify.abc",
    'abc_convert' : "./convert.abc",
    'opt_solver' : "/mnt/c/Users/giuse/Desktop/tools/optimathsat-linux/bin/optimathsat"
    }
config['LOG'] = {
    'enabled' : "True"
    }
