from unittest import TestCase

from networkx import OrderedGraph

from pfencoding.movablepf import MovableArchConstraints, MovablePenaltyFunction
from dwave_networkx.generators.chimera import chimera_graph
class MovableTest(TestCase):
    def test1(self):
        mpf = MovablePenaltyFunction(5, 3)
        g = OrderedGraph(chimera_graph(1,1))
        ac = MovableArchConstraints(g)
        for assertn in  map(str, ac.constraints(mpf)):
            print(assertn)
