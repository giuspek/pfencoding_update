from unittest import TestCase

from pfencoding.searchpf import search_pf
from pfencoding.utils import print_pf


def function1(t):
    (a, b, c) = t
    return (c == (a and b))

class TestExample2And2(TestCase):
    """Example encoding of and2 function"""

    def test_noancilla(self):


        func = (function1)

        model, pf = search_pf(3, 0, None, func)
        if model:
            print_pf(pf, model, func)

    def test_ancilla(self):

        model, pf = search_pf(3, 2, None, function1)
        if model:
            print_pf(pf, model, function1)
