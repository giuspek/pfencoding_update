from unittest import TestCase


from pfencoding.searchpf import search_pf_movable, search_pf_smallest
from pfencoding.utils import print_pf
from dwave_networkx.generators.chimera import chimera_graph

def majfunc(t):
    (a, b, c, d) = t
    return (d == ((b or c) if a else (b and c)))

class TestExampleMaj3(TestCase):
    """Example encoding of majority function"""
    def test(self):
        g = chimera_graph(1,1)



        model, pf = search_pf_movable(4,2, g, majfunc)
        print(model)
        print_pf(pf, model, majfunc)

    def test_smallest(self):
        g = chimera_graph(1,1, coordinates=True)

        model, pf = search_pf_smallest(4,2, g, majfunc)
        print(model)

        print_pf(pf, model, majfunc)








