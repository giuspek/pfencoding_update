

cdef extern from "nauty.h":
    int WORDSIZE
    ctypedef graph;
    ctypedef optionblk;
    void DEFAULTOPTIONS_GRAPH(optionblk)

    ctypedef statsblk;
    void nauty(graph*,int*,int*,set*,int*,optionblk*,
                  statsblk*,set*,int,int,int,graph*)