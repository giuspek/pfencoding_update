
from .nausparse cimport *
import networkx as nx
from libc.stdlib cimport malloc, free, calloc

cdef optionblk optblk

cdef class SparseGraph:
    cdef sparsegraph _graph
    cdef public list nodelist
    def __cinit__(self, nxgraph=None):
        cdef int edge_accum = 0
        SG_INIT(self._graph)
        if nxgraph is not None:
            SG_ALLOC(self._graph,nxgraph.number_of_nodes(), 2*nxgraph.number_of_edges(), "failed allocation")
            self._graph.nv = nxgraph.number_of_nodes()
            self._graph.nde = 2*nxgraph.number_of_edges()
            nodelist = list(nxgraph.nodes())

            for n1 in nodelist:
                i1 = nodelist.index(n1)
                self._graph.v[i1] = edge_accum
                self._graph.d[i1] = nxgraph.degree(n1)
                for n2 in nxgraph.neighbors(n1):
                    i2 = nodelist.index(n2)
                    assert edge_accum < self._graph.elen
                    self._graph.e[edge_accum] = i2
                    edge_accum += 1
            self.nodelist = nodelist
        else:
            self.nodelist = list()


    def __dealloc__(self):
        SG_FREE(self._graph)

    def get_orbits(self, colors=None):
        cdef statsblk stats
        DEFAULTOPTIONS_SPARSEGRAPH(optblk)
        optblk.getcanon = FALSE

        N = len(self.nodelist)
        cdef int* lab = <int*>calloc(N, sizeof(int))
        cdef int* ptn = <int*>calloc(N, sizeof(int))
        cdef int* orbits = <int*>calloc(N, sizeof(int))
        if colors is None:
            optblk.defaultptn=TRUE
        else:
            optblk.defaultptn=FALSE
            i = 0
            for color in colors:
                if len(color) == 0: raise ValueError("empty color not allowed")
                for v in color:
                    if i >= N: raise ValueError("out of bounds from colors")
                    lab[i] = self.nodelist.index(v)
                    ptn[i] = 1
                    i += 1
                ptn[i-1] = 0
            if i != N: raise ValueError("missing vertices in colors")

        sparsenauty(&(self._graph), lab, ptn, orbits, &optblk, &stats, NULL)

        ret = dict()

        for i in range(N):
            ret[self.nodelist[i]] = (self.nodelist[orbits[i]])

        free(lab)
        free(ptn)
        free(orbits)

        return ret

    def get_canon(self, colors=None):
        cdef statsblk stats
        DEFAULTOPTIONS_SPARSEGRAPH(optblk)
        optblk.getcanon = TRUE

        ret = SparseGraph()
        N = len(self.nodelist)
        cdef int* lab = <int*>calloc(N, sizeof(int))
        cdef int* ptn = <int*>calloc(N, sizeof(int))
        cdef int* orbits = <int*>calloc(N, sizeof(int))
        if colors is None:
            optblk.defaultptn=TRUE
        else:
            optblk.defaultptn=FALSE
            i = 0
            for color in colors:
                if len(color) == 0: raise ValueError("empty color not allowed")
                for v in color:
                    if i >= N: raise ValueError("out of bounds from colors")
                    lab[i] = self.nodelist.index(v)
                    ptn[i] = 1
                    i += 1
                ptn[i-1] = 0
            if i != N: raise ValueError("missing vertices in colors")

        sparsenauty(&(self._graph), lab, ptn, orbits, &optblk, &stats,&(ret._graph))
        ret.nodelist= [self.nodelist[lab[i]] for i in range(N)]
        

        assert self._graph.nv == ret._graph.nv
        assert ret._graph.nv == N
        assert self._graph.nde == ret._graph.nde

        #for i in range(self._graph.nv):
        #    print(self._graph.d[i], ret._graph.d[i])



        free(lab)
        free(ptn)
        free(orbits)

        return ret
    
    def get_canon_orbits(self, colors):
        cdef statsblk stats
        DEFAULTOPTIONS_SPARSEGRAPH(optblk)
        optblk.getcanon = TRUE
        
        ret = SparseGraph()
        N = len(self.nodelist)
        cdef int* lab = <int*>calloc(N, sizeof(int))
        cdef int* ptn = <int*>calloc(N, sizeof(int))
        cdef int* orbits = <int*>calloc(N, sizeof(int))
        if colors is None:
            optblk.defaultptn=TRUE
        else:
            optblk.defaultptn=FALSE
            i = 0
            for color in colors:
                if len(color) == 0: raise ValueError("empty color not allowed")
                for v in color:
                    if i >= N: raise ValueError("out of bounds from colors")
                    lab[i] = self.nodelist.index(v)
                    ptn[i] = 1
                    i += 1
                ptn[i-1] = 0
            if i != N: raise ValueError("missing vertices in colors")

        sparsenauty(&(self._graph), lab, ptn, orbits, &optblk, &stats,&(ret._graph))
        nodelist= [self.nodelist[lab[i]] for i in range(N)]
        orbitret = dict()

        for i in range(N):
            orbitret[self.nodelist[i]] = (self.nodelist[orbits[i]])


        assert self._graph.nv == ret._graph.nv
        assert ret._graph.nv == N
        assert self._graph.nde == ret._graph.nde

        #for i in range(self._graph.nv):
        #    print(self._graph.d[i], ret._graph.d[i])



        free(lab)
        free(ptn)
        free(orbits)

        return nodelist, orbitret

    def __eq__(self, other):
        return (0 != aresame_sg(&(self._graph), &((<SparseGraph?>other)._graph))
                )#and self.nodelist == (<SparseGraph?>other).nodelist)

    def __hash__(self):
        cdef unsigned long hsh = 0
        #for node in self.nodelist:
        #    hsh += hash(node)
        cdef unsigned long edgehash = 12345
        for i in range(self._graph.nv):
            for j in range(self._graph.d[i]):
                edgehash += self._graph.e[self._graph.v[i] + j]
            edgehash = 8253729 * edgehash + 2396403
        hsh += edgehash
        return hsh

    def to_networkx(self):
        ret = nx.Graph()
        for i in range(self._graph.nv):
            ret.add_node(self.nodelist[i])
        for i in range(self._graph.nv):
            for j in range(self._graph.d[i]):
                n2 = self._graph.e[self._graph.v[i] + j]
                ret.add_edge(self.nodelist[i], self.nodelist[n2])
        return ret

