import configparser

config = configparser.ConfigParser()

config['DEFAULT'] = {
    'abc' : "/usr/bin/berkeley-abc",
    'abc_rc' : "./src/abc.rc",
    'abc_simplify' : "./src/simplify.abc",
    'abc_convert' : "./src/convert.abc"
    }
config['LOG'] = {
    'enabled' : "True"
    }
