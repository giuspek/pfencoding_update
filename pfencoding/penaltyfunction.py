"Classes representing penalty functions"
from functools import reduce
from itertools import combinations

from smtutils.formula import Symbol, ValidNode
from typing import Iterable, Union, Optional
from six.moves import xrange

def vectproduct(xs, ys):
    # type: (Iterable[ValidNode], Iterable[ValidNode]) -> ValidNode
    "Vector product for symbols"

    def function1(t):
        (a, b) = t
        return a * b

    return reduce(lambda a,b: a+b,  map(function1,zip(xs, ys)))

def vectcouplings(coups, zs):
    # type: (Iterable[ValidNode], Iterable[ValidNode]) -> ValidNode
    "Sum of coupling terms"
    ret = [ c * a * b for (a,b),c in zip(combinations(zs, 2), coups)]
    return reduce(lambda a,b: a+b, ret)



class PenaltyFunction(object):
    "Class representing symbolic penalty functions"
    def __init__(self, nx, na):
        # type: (int, int) -> None
        "Initialize with number of x and ancilla variables"
        self.nx = nx
        self.na = na
        n = nx + na
        self.n = n

        self.define_spins(nx, na)
        self.define_theta(n)

        self.thetas = [self.offset] + self.biases + self.couplings
        self._full = self.offset + vectproduct(self.biases, self.zs) + vectcouplings(self.couplings, self.zs)

    def define_spins(self, nx, na):
        self.xs = [Symbol("Int", "x{}", i) for i in xrange(1, nx + 1)]
        self.as_ = [Symbol("Int", "a{}", i) for i in xrange(1, na + 1)]
        self.zs = self.xs + self.as_

    def define_theta(self, n):
        self.offset = Symbol("Real", "off")
        self.biases = [Symbol("Real", "bia{}", i) for i in xrange(1, n + 1)]
        self.couplings = [Symbol("Real", "coup{}_{}", i, j) for i, j in combinations(range(1, n + 1), 2)]

    def get_coupling(self, i, j):
        if i > j:
            j, i = i, j
        combs = list(combinations(range(len(self.zs) ), 2))
        return self.couplings[combs.index((i,j))]

    def value(self, assgnm=None):
        # type: (Optional[Union[list,dict]]) -> ValidNode
        "Return the symbolic value of the penalty funtion for a certain (partial) assignment"
        if assgnm is None:
            return self._full
        if type(assgnm) == list or type(assgnm) == tuple:
            asm = tuple(assgnm) + tuple(self.zs[len(assgnm):])
        elif type(assgnm) == dict:
            asm = [assgnm[i] if i in assgnm else self.zs[i] for i in xrange(self.n)]
        else:
            asm = assgnm
        return (self.offset + vectproduct(self.biases, asm) + vectcouplings(self.couplings, asm))


