"""Classes representing constraints over penalty functions"""
from itertools import product, combinations
from typing import Iterable, Optional, Tuple, Callable
from pfencoding.penaltyfunction import PenaltyFunction
from abc import abstractmethod
from smtutils.formula import ValidNode, Symbol, Op
import networkx as nx
from six.moves import reduce


def isingexpansion(n):
    # type: (int) -> Iterable
    "Return all possible states of a n-var Ising model"
    return product(*([(-1, 1)] * n))


def isingtobool(x):
    # type: (int) -> bool
    "Convert from (-1, 1) to (False, True)"
    return x == 1


class PfConstraint(object):
    "Abstract class representing a constraint over a pf"

    @abstractmethod
    def constraints(self, pf):
        # type: (PenaltyFunction) -> Iterable[ValidNode]
        "Generate the constraint for a specific pf"
        raise NotImplementedError


class RangeConstraint(PfConstraint):
    "Class representing range constraints on parameters"

    def __init__(self, biasrange=(-2, 2), couplrange=(-1, 1)):
        # type: (Optional[Tuple[int,int]], Optional[Tuple[int,int]]) -> None
        self.biarange = biasrange
        self.couplrange = couplrange

    def constraints(self, pf):
        biasmin, biasmax = self.biarange
        cmin, cmax = self.couplrange
        for bias in pf.biases:
            yield (bias >= biasmin) 
            yield (bias <= biasmax)
        for coupl in pf.couplings:
            coupl_min = (coupl >= cmin)
            coupl_min.couple_node = coupl
            yield coupl_min
            coupl_max = (coupl <= cmax)
            coupl_max.couple_node = coupl
            yield coupl_max


class ArchitectureConstraint(PfConstraint):
    "Class representing constraints on the induced graph"

    def __init__(self, graph):
        # type: (nx.Graph) -> None
        edges = graph.edges()
        nz = graph.number_of_nodes()
        assert set(graph.nodes()) == set(range(nz))
        self.noedges = frozenset((i,j) for (i,j) in combinations(range(nz),2)
                                 if (i,j) not in edges and (j,i) not in edges)

    def constraints(self, pf):
        edges = (self.noedges)
        for i, j in edges:
            new_op = (pf.get_coupling(i,j) == 0)
            new_op.set_discard()
            yield new_op


class ExpansionGapConstraint(PfConstraint):
    """Generate gap constraints by expanding variables"""

    def __init__(self, boolfunc, gmin):
        # type: (Callable[[Iterable[bool]], bool], ValidNode) -> None
        self.boolfunc = boolfunc
        self.gmin = gmin

    def constraints(self, pf):
        """As an iterator for speed, yields constraints for every possible assignments on xs an as"""
        boolfunc = self.boolfunc
        gmin = self.gmin
        precalc_biasesx = [(x, (str(pf.biases[x] * -1), None, str(pf.biases[x] * 1))) for x in range(pf.nx)]
        precalc_biasesa = [(x, (str(pf.biases[x] * -1), None, str(pf.biases[x] * 1))) for x in range(pf.nx, pf.nx + pf.na)]
        xcoupl = combinations(range(pf.nx), 2)
        xacoupl = product(range(pf.nx), range(pf.nx, pf.nx+pf.na))
        acoupl = combinations(range(pf.nx, pf.nx+pf.na), 2)
        def precalccoupl(i,j, pf):
            return (i, j, (str(pf.get_coupling(i,j)* -1),None, str(pf.get_coupling(i,j)* 1)))
        precalc_couplx = [precalccoupl(i,j, pf) for i,j in xcoupl]
        precalc_coupla = [precalccoupl(i,j, pf) for i,j in acoupl]
        precalc_couplxa = [precalccoupl(i,j, pf) for i,j in xacoupl]
        temp_idx = 1
        xexpansions = []
        for xvals in isingexpansion(pf.nx):
            first_part = (Op("+", 0, pf.offset, 
                *(choice[xvals[x]+1] for x, choice in precalc_biasesx), 
                *(choice[(xvals[i]*xvals[j])+1] for i, j, choice in precalc_couplx)
                ))
            fresh = Symbol("Real", f"fresh_{temp_idx}")
            temp_idx += 1
            new_op = (fresh == first_part)
            yield new_op
            xexpansions.append(fresh)
        
        aexpansions = []
        for avals in isingexpansion(pf.na):
            allvals = (None,) * pf.nx + avals
            ancilla_part = Op("+", 0, 0,
                    *(choice[allvals[x]+1] for x, choice in precalc_biasesa), 
                    *(choice[(allvals[i]*allvals[j])+1] for i, j, choice in
                        precalc_coupla))
            fresh = Symbol("Real", f"fresh_{temp_idx}")
            temp_idx += 1
            new_op = (fresh == ancilla_part)
            yield new_op
            aexpansions.append(fresh)

        for xvals, fresh in zip(isingexpansion(pf.nx), xexpansions):
            if boolfunc(list(map(isingtobool, xvals))):
                allpfs = []
                if pf.na == 0:
                    new_op = Op("=", fresh, 0)
                    yield new_op
                else:
                    for avals, fresh2 in zip(isingexpansion(pf.na), aexpansions):
                        fresh3 = Symbol("Real", f"fresh_{temp_idx}")
                        temp_idx += 1
                        allvals = xvals + avals
                        asfull = (Op("+", fresh, fresh2,
                    *(choice[(allvals[i]*allvals[j])+1] for i, j, choice in
                        precalc_couplxa)
                                ))
                        new_op = (fresh3 == asfull)
                        yield new_op
                        yield Op(">=", fresh3, 0)
                        allpfs.append(Op("=", fresh3, 0))
                    yield Op("or",  *allpfs)
            else:
                if pf.na == 0:
                    yield Op(">=", fresh,  gmin)
                else:
                    for avals, fresh2 in zip(isingexpansion(pf.na), aexpansions):
                        allvals = xvals + avals
                        asfull = Op("+", fresh, fresh2,
                        *(choice[(allvals[i]*allvals[j])+1] for i, j, choice in
                            precalc_couplxa)
                                )
                        new_op = Op(">=", asfull, gmin)
                        yield new_op


class MinimizeAncillas(PfConstraint):
    """Add a symbol counting the number of ancilla used"""

    def __init__(self, maxau):
        self.au = Symbol("Int", "ancilla_used")
        self.maxau = maxau

    def constraints(self, pf):
        """Constraints that define the number of ancilla used"""
        ancillaused = self.au
        yield ancillaused >= 0
        yield ancillaused <= self.maxau
        nx = pf.nx
        ncoupl = list(combinations(range(len(pf.zs)), 2))
        for i in range(pf.na):
            zi = nx + i
            yield ~(ancillaused <= i) | ((pf.biases[zi] == 0) &
                                         reduce(lambda a, b: a & b,
                                                [(coupl == 0) for coupl, (i, j) in zip(pf.couplings, ncoupl)
                                                 if i == zi or j == zi]))

    def minimize(self):
        """Variables to minimize"""
        return [self.au]