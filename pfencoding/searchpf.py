"""Utility functions to find penalty functions"""
from pfencoding.variableelimination import VariableEliminationConstraints
from smtutils.formula import SmtFormula
from smtutils.process import Solver, get_msat_path
from smtutils.parsing import SmtResponseParser
from pfencoding.constraints import RangeConstraint, ExpansionGapConstraint, ArchitectureConstraint, MinimizeAncillas, \
    PfConstraint
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.movablepf import MovableArchConstraints, MovablePenaltyFunction
from typing import Callable, Optional, List
import networkx
from pfencoding.enum_placements import generate_all_pos
from pfencoding.symmetries import AncillaSymBreak
from smtutils.formula import Symbol
from multiprocessing import Pool


def search_pf(nx, na, graph, func, extraconstr=tuple()):
    # type: (int, int, Optional[networkx.Graph], Callable) -> (dict, PenaltyFunction)
    """Search for a fixed-graph penalty function"""
    f = SmtFormula()
    pf = PenaltyFunction(nx, na)
    g = Symbol("Real", "g_min")
    constr_types = [RangeConstraint(), ExpansionGapConstraint(func, g)]
    if graph is not None:
        archcon = ArchitectureConstraint(graph)
        constr_types.append(archcon)
    for ctype in constr_types:
        for constr in ctype.constraints(pf):
            f.assert_(constr)
    f.maximize(g)

    slv = Solver(get_msat_path("optimathsat"))
    f.check_sat()
    f.directives.append("(set-model -1)")
    f.get_values(*pf.thetas)
    f.get_values(g)
    res = slv.run_formula(str(f))
    resp = SmtResponseParser(res)
    model = resp.model

    return model, pf


def search_pf_movable(nx, na, graph, func, extraconstr=tuple(), satisfy_gmin=None):
    # type: (int, int, networkx.Graph, Callable) -> (dict, PenaltyFunction)
    """Search for a penalty function with variable placement"""
    pf = MovablePenaltyFunction(nx, na)
    ograph = networkx.OrderedGraph(graph)
    nodes = list(ograph.nodes())

    g = Symbol("Real", "g_min")
    constraints = [
        RangeConstraint(),
        MovableArchConstraints(ograph),
        ExpansionGapConstraint(func, g),
        *extraconstr
    ]

    formula = SmtFormula()

    for cfact in constraints:
        for costraint in cfact.constraints(pf):
            formula.assert_(costraint)
    if satisfy_gmin is None:
        formula.maximize(g)
    else:
        formula.assert_(g == satisfy_gmin)

    formula.check_sat()
    #formula.directives.append("(set-model -1)")
    formula.get_values(*pf.thetas)
    formula.get_values(*pf.pos)
    formula.get_values(g)

    solver = Solver(get_msat_path("optimathsat"))
    res = solver.run_formula((formula))

    #print(res)
    pres = SmtResponseParser(res)
    if pres.errors:
        print(pres.errors)
    model = pres.model
    if model:
        for pos in pf.pos:
            pos = str(pos)
            model[pos] = nodes[model[pos] - 1]

    return model, pf


def search_pf_smallest(nx, na, graph, func, gap=2, extraconstr=tuple()):
    # type: (int, int, networkx.Graph, Callable, Optional[int], Optional[List[PfConstraint]]) -> (dict, PenaltyFunction)
    """Search for the smallest2 penalty function with variable placement"""
    pf = MovablePenaltyFunction(nx, na)
    ograph = networkx.OrderedGraph(graph)
    nodes = list(ograph.nodes())
    ma = MinimizeAncillas(na)
    constraints = [
        RangeConstraint(),
        MovableArchConstraints(ograph),
        ExpansionGapConstraint(func, (gap)),
        ma
    ]
    constraints.extend(extraconstr)

    formula = SmtFormula()

    for cfact in constraints:
        for costraint in cfact.constraints(pf):
            formula.assert_(costraint)

    #realau = Symbol("Real", "real_au")
    #formula.assert_(realau == (-ma.au))
    formula.minimize(ma.au)

    formula.check_sat()
    #formula.directives.append("(set-model -1)")
    formula.get_values(*pf.thetas)
    formula.get_values(*pf.pos)
    formula.get_values(ma.au)

    solver = Solver(get_msat_path("optimathsat"))
    print("QUI")
    print(str(formula))
    res = solver.run_formula(formula)

    # nothing to parse omt comments yet
    #print(res)
    pres = SmtResponseParser(res)
    if pres.errors:
        print(pres.errors)
    model = pres.model
    if model:
        for pos in pf.pos:
            pos = str(pos)
            model[pos] = nodes[model[pos] - 1]

    return model, pf


def search_pf_smallest_noarch(nx, na, func, extraconstr=tuple()):
    # type: (int, int, Callable, Optional[List[PfConstraint]]) -> (dict, PenaltyFunction)
    """Search for the smallest2 penalty function with variable placement"""
    pf = PenaltyFunction(nx, na)
    ma = MinimizeAncillas()
    constraints = [
        RangeConstraint(),
        ExpansionGapConstraint(func, 2),
        ma
    ]
    constraints.extend(extraconstr)

    formula = SmtFormula()

    for cfact in constraints:
        for costraint in cfact.constraints(pf):
            formula.assert_(costraint)

    formula.minimize(ma.au)

    formula.check_sat()
    formula.directives.append("(set-model -1)")
    formula.get_values(*pf.thetas)
    formula.get_values(ma.au)

    solver = Solver(get_msat_path("optimathsat"))
    res = solver.run_formula(formula)

    # nothing to parse omt comments yet
    res = "\n".join(x for x in res.splitlines() if not x.startswith("#"))
    pres = SmtResponseParser(res)
    model = pres.model
    return model, pf

def _vsearch(args):
        model, pf = search_pf_ve(*args[0])
        return model, pf, args[1]

import sys
from datetime import datetime
        
def search_pf_ve_all_pos(nx, na, graph, func, symmetric_vars=None, inner_vars=None, extraconstr=tuple(), gap=2):

    if inner_vars is None:
        inner_vars = set()

    if symmetric_vars is None:
        symmetric_vars = [1] * nx
    if na >0:
        symmetric_vars += [na]

    pool = Pool()
    def call_list():
      for positioning in generate_all_pos(graph, symmetric_vars):
        if any(nxpos in inner_vars for nxpos in positioning[:nx]): continue
        #print(positioning)
        posgraph = networkx.Graph(graph)
        posgraph.remove_nodes_from(n for n in graph.nodes 
                if n not in positioning)
        if not networkx.is_connected(posgraph): continue
        posgraph = networkx.relabel_nodes(posgraph, 
                dict(zip(positioning, range(nx + na))), copy=True)
        # networkx.relabel_nodes(posgraph, dict(zip(range(nx+na), positioning)), copy=False)
        yield ((nx, na, posgraph, func, extraconstr, gap), positioning)

    for model,pf, positioning  in pool.imap_unordered(_vsearch, call_list()):
        print(f"{datetime.now().time()}: SMT call ended, result {model}")
        sys.stdout.flush()
        if model:
            pool.close()
            pool.terminate()
            return model, pf, positioning
    return None, None, None

def search_pf_ve(nx, na, graph, func, extraconstr=tuple(), gap=2):
    assert graph.number_of_nodes() == nx + na, "incorrect graph"

    sub_constr = [ VariableEliminationConstraints(graph, nx, na, func, gap), 
            ArchitectureConstraint(graph),
            RangeConstraint(), 
            *extraconstr
                  ]
    
    f = SmtFormula()
    pf = PenaltyFunction(nx, na)

    for ctype in sub_constr:
        for constr in ctype.constraints(pf):
            f.assert_(constr)

    slv = Solver(get_msat_path("optimathsat"))

    f.check_sat()
    f.get_values(*pf.thetas)

    res = slv.run_formula(f)
    resp = SmtResponseParser(res)
    if resp.errors:
        print(f"SMT errors: {resp.errors} {resp.result}")
    model = resp.model

    return model, pf
