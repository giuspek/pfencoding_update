from pfencoding.constraints import isingexpansion, isingtobool
from itertools import combinations


def print_table(offset, h, J, nx, na, tfunc):
    """Print all minimum values for a penalty function"""
    assert len(h) == nx + na
    assert len(J)* 2 + len(h) == len(h)  * len(h), (len(J), len(h))
    for xs in isingexpansion(nx):
        minv = None
        for a_ in isingexpansion(na):
            zs = xs + a_
            sm = (offset
                  + sum(a*b for a, b in zip(zs, h))
                  + sum(a * i * j for a, (i,j) in zip(J, combinations(zs, 2)))
                  )
            if minv is None or sm < minv:
                minv = sm
        if na == 0:
            zs = xs
            minv = (offset
                  + sum(a * b for a, b in zip(zs, h))
                  + sum(a * i * j for a, (i, j) in zip(J, combinations(zs, 2)))
                  )
        print(xs, minv, tfunc(list(map(isingtobool, xs))))


def print_pf(pf, model, func):
    """Print all minimum values for a penalty function"""
    print_table(model[str(pf.offset)], [model[str(x)] for x in pf.biases],
                [model[str(x)] for x in pf.couplings], pf.nx, pf.na, func)