from io import StringIO
from typing import TextIO

import sexpdata
from fractions import Fraction
from six import print_, u, b

def parseRational(v):
    if hasattr(v, '__len__'):
        if v[0].value() == "-":
            return -parseRational(v[1])
        if v[0].value() == "/":
            return Fraction(parseRational(v[1]), parseRational(v[2]))
        else:
            raise  ValueError("error parsing rational: {!r}".format(v))
    else:
        return v

def parse_lines(lines):
    json_part = []
    for line in lines:
        if line.startswith("#") or line.strip() == "":
            continue
        else:
            json_part.append(line)

    json_part =  "\n".join(json_part)

    try:
        for obj in  sexpdata.parse(json_part):
            yield obj
    except:
        raise ValueError("error parsing JSON: {!r}".format(json_part))

class SmtResponseParser(object):
    def __init__(self, obj):
        if not isinstance(obj, TextIO):
            obj = iter(obj.splitlines())

        self.result = "unknown"
        self.model = dict()
        self.errors = list()

        self._parse_stream(obj)


    def _parse_stream(self, lines):
        objs = parse_lines(lines)
        for content in objs:
            if isinstance(content, sexpdata.Symbol):
                self.result = content.value()
            elif content[0] == sexpdata.Symbol("error"):
                self.errors.append(content[1])
            else:
                for k, v in content:
                    key = sexpdata.dumps(k)
                    self.model[key] = parseRational(v)
