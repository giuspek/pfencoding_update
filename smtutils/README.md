Smtutils: utilities for SMTLIB formulas
=======================================

Lightweight library to create SMT formulas, spawn SMT solver
processes and parse results.